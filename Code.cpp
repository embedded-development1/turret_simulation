
#include <Servo.h>
#include <LiquidCrystal_I2C.h>

#define VERT_PIN A3
#define HORZ_PIN A2

Servo turnArm;
Servo angleArm;
LiquidCrystal_I2C lcd(0x27,20,4);

float pos = 90.0;
float angle = 180.0;
float step = 1.0;
int ammo = 10;

void setup() {
  // put your setup code here, to run once:
  pinMode(VERT_PIN, INPUT);
  pinMode(HORZ_PIN, INPUT);
  pinMode(7, INPUT_PULLUP);
  pinMode(8, INPUT_PULLUP);

  pinMode(6, OUTPUT);
  pinMode(4, OUTPUT);

  lcd.init();  
  lcd.backlight();
  setLCD();

  turnArm.attach(3);
  angleArm.attach(5);
  turnArm.write(pos);
  angleArm.write(angle);
}

void loop() {
  int horz = analogRead(HORZ_PIN);
  int vert = analogRead(VERT_PIN);
  // put your main code here, to run repeatedly:
  if (vert < 300){//Joystick down
    if (angle<180)
    {
      angleArm.write(angle);
      angle+=step;
      delay(5);
    }
  }
  if (vert > 700) {//Joystick up
    if (angle>90)
    {
      angleArm.write(angle);
      angle-=step;
      delay(5);
    }
  }
  if (horz < 300){//Joystick right
    if (pos<180)
    {
      turnArm.write(pos);
      pos+=step;
      delay(5);
    }
  }
  if (horz > 700){//Joystick left
    if (pos>0)
    {
      turnArm.write(pos);
      pos-=step;
      delay(5);
    }
  }
  if(!digitalRead(7)){
    if(ammo>0){
      lcd.setCursor(6,0);
      lcd.print("Warning!");
      displayAngle();
      displayPos();
      Alarm();
      lcd.setCursor(6,0);
      lcd.print("Shooting!");
      Shooting();
    }
    else{
      lcd.setCursor(6,0);
      lcd.print("No Ammo!");
      delay(1000);
      lcd.clear();
      setLCD();
    }
  }
}

void Alarm(){
  digitalWrite(6, HIGH);
  tone(4, 262);
  delay(300);
  digitalWrite(6, LOW);
  noTone(4);
  delay(300);
  digitalWrite(6, HIGH);
  tone(4, 262);
  delay(300);
  digitalWrite(6, LOW);
  noTone(4);
  delay(300);
  digitalWrite(6, HIGH);
  tone(4, 262);
  delay(300);
  digitalWrite(6, LOW);
  noTone(4);
  delay(300);
}

void Shooting(){
  digitalWrite(6, HIGH);
  tone(4, 50);
  delay(1000);
  digitalWrite(6, LOW);
  noTone(4);
  lcd.setCursor(4,0);
  lcd.print("Shoot fired!");
  ammo--;
  delay(1000);
  setLCD();
}

void setLCD(){
  lcd.clear();
  lcd.setCursor(6,0);
  lcd.print("Turret!");
  lcd.setCursor(6,3);
  lcd.print("Ammo: " + String(ammo));
}

void displayAngle(){
  lcd.setCursor(3,1);
  float displayangle = (angle - 180) * -1;
  lcd.print("Angle:"+ String(displayangle));
}
void displayPos(){
  lcd.setCursor(0,2);
  float displayPos = pos - 90;
  if(displayPos < 0){
    lcd.print("Direction:" + String(displayPos * -1) + "left");
    lcd.setCursor(20,2);
    lcd.print(" ");
  }
  else if(displayPos > 0){
    lcd.print("Direction:" + String(displayPos) + "right");
  }
  else{
    lcd.print("Direction:Forward");
  }
}
